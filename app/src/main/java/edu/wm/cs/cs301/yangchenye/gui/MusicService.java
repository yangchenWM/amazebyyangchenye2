package edu.wm.cs.cs301.yangchenye.gui;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;

public class MusicService extends Service {
    MediaPlayer player;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        player = MediaPlayer.create(this, R.raw.g); //select music file
        player.setLooping(true); //set looping
        player.setVolume(100, 100);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("started", "Media Player started!");
        player.start();
        if (!player.isLooping()){
            Log.d("problem:", "Failed to Play Music");
        }
        return Service.START_NOT_STICKY;
    }

    public void onDestroy() {
        Log.d("destroyed", "Media Player ended!");
        player.stop();
        player.release();
        stopSelf();
        super.onDestroy();
    }

}