package edu.wm.cs.cs301.yangchenye.generation;


import android.os.Handler;
import android.widget.ProgressBar;

import edu.wm.cs.cs301.yangchenye.gui.Constants;

/**
 * An order describes functionality needed to order a maze from
 * the maze factory. It allows for asynchronous production 
 * with a mechanism to deliver a MazeConfiguration.
 * 
 * @author Peter Kemper
 *
 */
public interface Order {
	/**
	 * Gives the required skill level, range of values 0,1,2,...,15
	 */
	int getSkillLevel() ;
	/** 
	 * Gives the requested builder algorithm, possible values 
	 * are listed in the Builder enum type.
	 */
	Constants.Builder getBuilder() ;

	/**
	 * Describes if the ordered maze should be perfect, i.e. there are 
	 * no loops and no isolated areas, which also implies that 
	 * there are no rooms as rooms can imply loops
	 */
	boolean isPerfect() ;
	/**
	 * Delivers the produced maze. 
	 * This method is called by the factory to provide the 
	 * resulting maze as a MazeConfiguration.
	 * @param mazeConfig
	 */
	void deliver(Maze mazeConfig) ;

	/**
	 * provides the worker class a reference to the handler of the UI
	 * Thread through which it can update the progress bar
	 */
	Handler getHandler();

	/**
	 * provides a reference to the progress bar
	 */
	ProgressBar getProgressBar();

	/**
	 * inform the factory what seed to use
	 * @return
	 */
	int getSeed();
}
