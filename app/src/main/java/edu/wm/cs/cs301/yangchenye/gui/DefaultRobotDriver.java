package edu.wm.cs.cs301.yangchenye.gui;

import android.os.Handler;
import android.util.Log;

import edu.wm.cs.cs301.yangchenye.generation.Distance;

/**
 * This class inplements the robot driver interface, which has the responsibility to
 * provides functionality of a robot driver that drives a robot to escape from the maze
 * Specific robot driver classes should extend this default class and override the
 * driving method only
 *
 *
 * @author yangchen
 *
 */
public class DefaultRobotDriver implements RobotDriver{

    boolean started;

    /**
     * the robot which this driver operarates on
     */
    protected Robot robot;

    /**
     * width of the maze to play with
     */
    protected int width;

    /**
     * height of the maze to play with
     */
    protected int height;

    /**
     * informs the driver whether it should keep running or not
     */
    private boolean keepRunning = true;

    /**
     * distance matrix of the maze
     */
    protected Distance distance;

    Handler handler;

    public DefaultRobotDriver() {
        started = false;
    }

    @Override
    public void setRobot(Robot r) {
        //pre-condition robot cannot be null
        assert r != null;
        started = true;
        robot = r;

    }

    @Override
    public void setDimensions(int width, int height) {
        this.width = width;
        this.height = height;

    }

    @Override
    public void setDistance(Distance distance) {
        this.distance = distance;

    }

    @Override
    //wait to be implemented by each particular robot driver
    //behavior depends on each class
    public void triggerUpdateSensorInformation() {
        throw new RuntimeException("not implemented");
    }

    @Override
    //wait to be implemented by each particular robot driver class
    public boolean drive2Exit() throws Exception {
        throw new RuntimeException("not implemented");
    }

    @Override
    public void run() {
        Log.i("Thread:", "Driver Thread start running");

        while (keepRunning && !robot.hasStopped() && !robot.hasWon()){
            try {
                drive2Exit();
            } catch (Exception e) {
                Log.d("Exception", "Some Problem caused driver to crash");
            }
        }

    }

    @Override
    public void stopRunning(){
        keepRunning = false;
    }

    @Override
    public void resumeRunning() {
        keepRunning = true;
    }

    @Override
    public void setHandler(Handler handler) {
        this.handler = handler;
    }

}

