package edu.wm.cs.cs301.yangchenye.gui;

import android.content.Context;
import android.os.Handler;
import android.widget.ProgressBar;
import android.widget.TextView;

import edu.wm.cs.cs301.yangchenye.generation.CardinalDirection;
import edu.wm.cs.cs301.yangchenye.gui.Constants.UserInput;
import edu.wm.cs.cs301.yangchenye.generation.Maze;

/**
 * This is a default implementation of the State interface
 * with methods that do nothing but providing runtime exceptions
 * such that subclasses of this class can selectively override
 * those methods that are truly needed.
 *
 * @author Peter Kemper
 *
 */
public class DefaultState implements State {

    @Override
    public void start(Context context, MazePanel panel) {
        throw new RuntimeException("DefaultState:using unimplemented method");
    }

    @Override
    public void setMazeConfiguration(Maze config) {
        throw new RuntimeException("DefaultState:using unimplemented method");
    }

    @Override
    public boolean keyDown(UserInput key, int value) {
        throw new RuntimeException("DefaultState:using unimplemented method");
    }

    @Override
    public void setRobot(Robot robot) {
        throw new RuntimeException("DefaultState:using unimplemented method");
    }

    @Override
    public Maze getMazeConfig() {
        throw new RuntimeException("DefaultState:using unimplemented method");
    }

    @Override
    public int[] getCurrentPosition() {
        throw new RuntimeException("DefaultState:using unimplemented method");
    }

    @Override
    public CardinalDirection getCurrentDirection() {
        throw new RuntimeException("DefaultState:using unimplemented method");
    }

    @Override
    public void setProgressBar(ProgressBar progressBar){
        throw new RuntimeException("DefaultState:using unimplemented method");
    }

    @Override
    public void setHandler(Handler handler) {
        throw new RuntimeException("DefaultState:using unimplemented method");
    }

    @Override
    public void setTextView(TextView text) {
        throw new RuntimeException("DefaultState:using unimplemented method");
    }


}

