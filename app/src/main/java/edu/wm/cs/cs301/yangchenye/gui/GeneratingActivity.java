package edu.wm.cs.cs301.yangchenye.gui;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.transition.Slide;
import android.util.Log;
import android.widget.ProgressBar;

import edu.wm.cs.cs301.yangchenye.generation.Factory;
import edu.wm.cs.cs301.yangchenye.generation.Maze;
import edu.wm.cs.cs301.yangchenye.generation.MazeFactory;
import edu.wm.cs.cs301.yangchenye.generation.Order;

/**
 * This class has the responsibility to handle maze generation. It displays a front-end thread
 * together with a back-end thread that generates a maze.
 *
 * @author Yangchen Ye
 */
public class GeneratingActivity extends AppCompatActivity implements Order{

    /**
     * global variable, containing the maze configuration to use
     */
    public static Maze maze;

    ///////////////////UI update field////////////////////////////
    /**
     * the progress bar in the front end; updated by the back end generation programs
     */
    private ProgressBar progressBar;

    /**
     * the handler for the worker thread to use
     */
    private Handler handler;

    ///////////////maze generation related field////////////////////

    /**
     * the complexity of maze
     */
    private int skillLevel = 0;

    /**
     * builder
     */
    private Constants.Builder builder;

    /**
     * driver information, passed to the next activity
     */
    private int driverType = 0;

    /**
     * The seed of maze generation
     */
    private int seed;

    /**
     * tell if the used chose explore or revisit
     */
    Boolean newMaze;

    /**
     * Use a factory to handle the generation in a background thread
     */
    private Factory factory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generating);


        setUpOrderPreference();

        factory = new MazeFactory();
        factory.order(this);

        getWindow().setExitTransition(new Slide());

    }

    /**
     * This method set up the necessary information for the activity to be used as an order object
     */
    private void setUpOrderPreference(){

        //provides reference for background thread to access and update
        progressBar = findViewById(R.id.progressBar_generating);
        handler = new Handler();

        Intent intent = getIntent();
        //retrieve information from intent
        driverType = intent.getIntExtra(Constants.DRIVER_KEY, 0); //driver
        skillLevel = intent.getIntExtra(Constants.SKILL_LEVEL_KEY, 0);  //skill level
        newMaze = intent.getBooleanExtra(Constants.GENERATE_NEW_MAZE, false); //explore or revisit
        //type of the builder, get from AMazeActivity
        int builderType = intent.getIntExtra(Constants.BUILDER_KEY, 0);
        builder = createBuilder(builderType); //builder

        //get preference object
        SharedPreferences mazePreferences = getPreferences(AppCompatActivity.MODE_PRIVATE);
        SharedPreferences.Editor preferenceEditor = mazePreferences.edit();

        //use "builder"+"size" as a key; generation-seed as a value
        String preference_key = String.valueOf(builderType) + skillLevel;

        if (!newMaze && mazePreferences.contains(preference_key)) {
            //revisit and preference exists, get seed from preference
            seed = mazePreferences.getInt(preference_key, 1);

        } else {
            //preference doesn't exist or explore
            seed = (int) (Math.random() * 100); //randomly choose a seed
            preferenceEditor.putInt(preference_key, seed); //store it to the preference for future use
            preferenceEditor.apply();
        }
    }

    /**
     * create the builder type from the integer message
     * @param builderType
     * @return
     */
    private Constants.Builder createBuilder(int builderType){
        return Constants.Builder.getBuilder(builderType);
    }

    private void navigateToPlayingManually(){

        Log.v("Generating Success: ", "Game about to start");

        Intent intent = new Intent(this, PlayManuallyActivity.class);

        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());

        finish();
    }

    private void navigateToPlayingAnimation(){

        Log.v("Generating Success: ", "Game about to start");

        Intent intent = new Intent(this, PlayAnimationActivity.class);
        intent.putExtra(Constants.DRIVER_KEY, driverType);

        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());

        finish();

    }

    @Override
    public void onBackPressed(){
        Log.v("Back: ", "Return to Title");

        super.onBackPressed();
        factory.cancel();  //cancel the order to stop the thread
    }

    //////////////////////////////////Instantiating Order functions///////////////////////////////

    @Override
    public int getSkillLevel() {
        return skillLevel;
    }

    @Override
    public Constants.Builder getBuilder() {
        return builder;
    }

    @Override
    public boolean isPerfect() {
        return false;
    }

    @Override
    public void deliver(Maze mazeConfig) {

        //store the generated maze
        maze = mazeConfig;

        handler.post(new Runnable() {
            @Override
            public void run() {
                //navigate to play
                if (driverType == Constants.Driver.Manual.getValue()){
                    navigateToPlayingManually();
                } else{
                    navigateToPlayingAnimation();
                }
            }
        });


    }

    @Override
    public Handler getHandler(){
        return handler;
    }

    @Override
    public ProgressBar getProgressBar(){
        return progressBar;
    }

    @Override
    public int getSeed(){
        return seed;
    }

}
