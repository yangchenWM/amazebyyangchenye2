package edu.wm.cs.cs301.yangchenye.gui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

/**
 * This class provides the graphics functionality of the android app, all graphical update should
 * be performed through this class
 *
 * @author Yangchen Ye
 */
public class MazePanel extends View{

    //objects needed to perform custom drawing
    private Canvas canvas;
    private Paint paint;
    private Bitmap bitmap;

    //resource object: provides ability to draw certain pattern
    //for each parts of the maze
    private BitmapShader skyShader;
    private BitmapShader wallShader;
    private BitmapShader groundShader;

    public MazePanel(Context context, AttributeSet attr){
        super(context, attr);
        bitmap = Bitmap.createBitmap(Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT, Bitmap.Config.ARGB_8888);
        canvas = new Canvas(bitmap);
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStrokeWidth(5);

        skyShader = new BitmapShader(BitmapFactory.decodeResource(context.getResources(), R.drawable.sun), Shader.TileMode.CLAMP, Shader.TileMode.REPEAT);
        wallShader = new BitmapShader(BitmapFactory.decodeResource(context.getResources(), R.drawable.wall), Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        groundShader = new BitmapShader(BitmapFactory.decodeResource(context.getResources(), R.drawable.road), Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);

    }

    @Override
    public void onDraw(Canvas canvas){
        super.onDraw(canvas);

        canvas.drawBitmap(bitmap, 0, 0, paint);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Log.v("Chart onMeasure w", MeasureSpec.toString(widthMeasureSpec));
        Log.v("Chart onMeasure h", MeasureSpec.toString(heightMeasureSpec));

        int desiredWidth = getSuggestedMinimumWidth() + getPaddingLeft() + getPaddingRight();
        int desiredHeight = getSuggestedMinimumHeight() + getPaddingTop() + getPaddingBottom();

        setMeasuredDimension(measureDimension(desiredWidth, widthMeasureSpec),
                measureDimension(desiredHeight, heightMeasureSpec));
    }

    private int measureDimension(int desiredSize, int measureSpec) {
        int result;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            result = desiredSize;
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize);
            }
        }

        if (result < desiredSize){
            Log.e("View", "The view is too small, the content might get cut");
        }
        return result;
    }

    /**
     * Method to draw the buffer image on a graphics object that is
     * obtained from the superclass.
     * Warning: do not override getGraphics() or drawing might fail.
     */
    public void update() {
        invalidate();
    }
    /////////////////////////////////////maze-related graphic functionalities/////////////////////////////////////

    /**
     * draw a line on the graphic object the panel is holding on
     * @param startX
     * @param startY
     * @param i
     * @param startY2
     */
    public void drawLine(int startX, int startY, int i, int startY2) {
        canvas.drawLine(startX, startY, i, startY2, paint);
    }

    /**
     * fill in an oval space in the graphic object the panel is holding on
     * @param i
     * @param j
     * @param diameter
     * @param diameter2
     */
    public void fillOval(int i, int j, int diameter, int diameter2) {
        canvas.drawOval(i, j, i + diameter, j + diameter2, paint);
    }


    /**
     * overload, set the color according to an rgb value
     * @param r
     * @param g
     * @param b
     */
    public void setColor(int r, int g, int b) {
        int color = Color.argb(255, r, g, b);
        paint.setColor(color);
    }

    /**
     * fill in a rectangular shape of the current graphics object
     * @param i
     * @param j
     * @param width
     * @param height
     */
    public void fillRect(int i, int j, int width, int height) {
        canvas.drawRect(i, j, i + width, j + height, paint);
    }

    /**
     * draw a string
     * @param string
     * @param i
     * @param j
     */
    public void drawString(String string, int i, int j) {
        canvas.drawText(string, i, j, paint);
    }

    /**
     * fill in a polygon
     * @param xps
     * @param yps
     * @param i
     */
     public void fillPolygon(int[] xps, int[] yps, int i) {
        Path polygonPath = new Path();
        polygonPath.reset();
        polygonPath.moveTo(xps[0], yps[0]);
        for (int x = 1; x < i; ++x){
            polygonPath.lineTo(xps[x], yps[x]);
        }
        polygonPath.lineTo(xps[0], yps[0]);

        canvas.drawPath(polygonPath, paint);
    }

    /**
     * remove the current shader of the paint, draw pure color instead
     */
    public void removeShader(){
         paint.setShader(null);
    }

    /**
     * used to draw the sky
     */
    public void setSkyShader() {
        paint.setShader(skyShader);
    }

    /**
     * set the appropriate shader for drawing the wall
     */
    public void setWallShader(){
         paint.setShader(wallShader);
    }

    /**
     * set the appropriate shader for drawing the ground
     */
    public void setGroundShader(){
        paint.setShader(groundShader);
    }

}
