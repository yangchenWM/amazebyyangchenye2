package edu.wm.cs.cs301.yangchenye.gui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import edu.wm.cs.cs301.yangchenye.generation.Maze;

/**
 * This class has the responsibility to handle the maze playing process with a robot driver algorithm
 * operating at the back end
 *
 * @author Yangchen Ye
 */
public class PlayAnimationActivity extends AppCompatActivity {

    /**
     * the maze configuration
     */
    private Maze maze;

    /**
     * the state object that handles user interaction
     */
    private State statePlaying;

    /**
     * the robot object to play the maze
     */
    private Robot robot;

    /**
     * The driver to operate the robot
     */
    private RobotDriver driver;

    /**
     * Handler to control driver pause and start
     */
    private Handler handler;

    /**
     * worker thread to drive the robot
     */
    private Thread driverThread;

    //threads for triggering sensor failure
    private Thread leftThread;
    private Thread rightThread;
    private Thread forwardThread;
    private Thread backwardThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_animation);

        //retrieve maze configuration
        maze = GeneratingActivity.maze;
        GeneratingActivity.maze = null; //destroy global variable
        handler = new Handler();

        //set ui-configuration
        setUpToolBar();

        //initialize play state and robot
        initStatePlayingAndRobot();
        //initialize driver
        initDriver();

        //start
        statePlaying.start(this, (MazePanel)findViewById(R.id.view));

        //show map
        statePlaying.keyDown(Constants.UserInput.ToggleLocalMap, 0);
        statePlaying.keyDown(Constants.UserInput.ToggleFullMap, 0);
        statePlaying.keyDown(Constants.UserInput.ToggleSolution, 0);

        driverThread = null;
    }

    private void setUpToolBar(){
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    /**
     * initialize the state of the game
     */
    private void initStatePlayingAndRobot(){

        ProgressBar progressBar =  findViewById(R.id.energy_remaining);
        TextView text = findViewById(R.id.energy);
        //set up game
        statePlaying = new StatePlaying();
        ((StatePlaying) statePlaying).driverMode = true;
        robot = new BasicRobot();
        statePlaying.setMazeConfiguration(maze);
        statePlaying.setRobot(robot);
        statePlaying.setHandler(handler);
        statePlaying.setProgressBar(progressBar);
        statePlaying.setTextView(text);
        //set up robot
        robot.setMaze(statePlaying);

    }

    /**
     * set up driver according to the given message
     */
    private void initDriver(){
        //set the driver to use in this game
        driver = createDriver(getIntent().getIntExtra(Constants.DRIVER_KEY, 1));
        driver.setDimensions(maze.getWidth(), maze.getHeight());
        driver.setDistance(maze.getMazedists());
        driver.setRobot(robot);
    }

    /**
     * create the driver from given algorithm
     * @param driverType specifying which driver to use
     * @return a reference to the created driver object
     */
    private RobotDriver createDriver(int driverType){
        Constants.Driver driver = Constants.Driver.getDriver(driverType);
        if (driver == Constants.Driver.WallFollower)
            return new WallFollower();
        if (driver == Constants.Driver.Wizard)
            return new Wizard();
        return null;
    }

    /**
     * the game is won, navigate to the winning activity
     */
    public void navigateToWinning(){
        Log.v("Game Won", "Navigate to Winning Activity");

        shutDownAllThread();

        Intent intent = new Intent(this, WinningActivity.class);
        transferEndInformation(intent);
        startActivity(intent);

        finish();
    }

    /**
     * game is lost, navigate to according activity
     */
    public void navigateToLosing(){
        Log.v("Game Lost", "Navigate to Winning Activity");

        shutDownAllThread();

        Intent intent = new Intent(this, LosingActivity.class);
        transferEndInformation(intent);
        startActivity(intent);

        finish();
    }

    /**
     * helper method to transfer the energy, pathlength and shortest possible path via the intent
     * @param intent
     */
    private void transferEndInformation(Intent intent){
        intent.putExtra(Constants.ENERGY_CONSUMPTION_KEY, robot.getEnergyConsumption());
        intent.putExtra(Constants.PATH_LENGTH_KEY, robot.getOdometerReading());
        int[] startPosition = maze.getMazedists().getStartPosition();
        int[] exitPosition = maze.getMazedists().getExitPosition();
        int shortestPossiblePath = Math.abs(startPosition[0]-exitPosition[0]) + Math.abs(startPosition[1] - exitPosition[1]) + 1;
        intent.putExtra(Constants.SHORTEST_POSSIBLE_PATH_KEY,
                shortestPossiblePath);
    }
    /////////////////////Button for triggering map and solution//////////////////\

    /**
     * respond to user's click of the wall button; toggle wall display
     * @param view
     */
    public void wallButtonClicked(View view){

        Log.v("wall", "Toggle Seeable Walls");
        statePlaying.keyDown(Constants.UserInput.ToggleLocalMap, 0);

    }

    /**
     * respond to user's click of the wall button; toggle full map display
     * @param view
     */
    public void fullMapButtonClicked(View view){

        Log.v("fullMap", "Toggle Full Map");
        statePlaying.keyDown(Constants.UserInput.ToggleFullMap, 0);

    }

    /**
     * respond to user's click of the wall button; toggle solution display
     * @param view
     */
    public void solutionButtonClicked(View view){

        Log.v("solution","Toggle Solution");
        statePlaying.keyDown(Constants.UserInput.ToggleSolution, 0);

    }

    /**
     * respond to user's click of the scale up button
     * @param view
     */
    public void scaleUpButtonClicked(View view){

        Log.v("scaleUp","Scale Up Map");
        statePlaying.keyDown(Constants.UserInput.ZoomIn, 0);

    }

    /**
     * respond to user's click of the scale down button
     * @param view
     */
    public void scaleDownButtonClicked(View view){

        Log.v("scaleDown","Scale Down Map");
        statePlaying.keyDown(Constants.UserInput.ZoomOut, 0);

    }

    ///////////////////Pause and Start Button//////////////////////////////

    /**
     * pause/start the driving when user clicked the paus/start button
     * @param view
     */
    public void pauseStartButtonClicked(View view){

        Log.v("pause/start", "Pause Or Start Driving");
        if (driverThread == null){
            driver.resumeRunning();
            driverThread = new Thread(driver);
            driverThread.start();
        } else{
            driver.stopRunning();
            driverThread = null;
        }
    }

    /////////////////////Sensor Failure Button//////////////////////////////

    /**
     * toggle the left thread to work or stop
     * @param view
     */
    public void leftClicked(View view){

        if (leftThread == null){
            Log.v("Sensor: ", "Begin Fail/Repair Left Sensor");
            leftThread = new Thread(new RobotFailer(robot, Robot.Direction.LEFT));
            leftThread.start();
        }
        else{
            Log.v("Sensor: ", "Left Thread closed");
            leftThread.interrupt();
            leftThread = null;
        }
    }

    /**
     * toggle the forward thread to work or stop
     * @param view
     */
    public void forwardClicked(View view){
        if (forwardThread == null){
            Log.v("Sensor: ", "Begin Fail/Repair Forward Sensor");
            forwardThread = new Thread(new RobotFailer(robot, Robot.Direction.FORWARD));
            forwardThread.start();
        }
        else{
            Log.v("Sensor: ", "Forward Thread closed");
            forwardThread.interrupt();
            forwardThread = null;
        }
    }

    /**
     * toggle the right thread to work or stop
     * @param view
     */
    public void rightClicked(View view){
        if (rightThread == null){
            Log.v("Sensor: ", "Begin Fail/Repair Right Sensor");
            rightThread = new Thread(new RobotFailer(robot, Robot.Direction.RIGHT));
            rightThread.start();
        }
        else{
            Log.v("Sensor: ", "Right Thread closed");
            rightThread.interrupt();
            rightThread = null;
        }
    }

    /**
     * toggle the backward thread to work or stop
     * @param view
     */
    public void backClicked(View view){
        if (backwardThread == null){
            Log.v("Sensor: ", "Begin Fail/Repair Backward Sensor");
            backwardThread = new Thread(new RobotFailer(robot, Robot.Direction.BACKWARD));
            backwardThread.start();
        }
        else{
            Log.v("Sensor: ", "Backward Thread closed");
            backwardThread.interrupt();
            backwardThread = null;
        }
    }

    /**
     * shut down all running thread; used when the activity finishes.
     */
    private void shutDownAllThread(){
        Log.v("Sensor: ", "All Threads shut down");

        //stop driver thread
        driver.stopRunning();
        driverThread = null;

        //stop sensor threads
        if (leftThread != null){
            leftThread.interrupt();
            leftThread = null;
        }
        if (rightThread != null){
            rightThread.interrupt();
            rightThread = null;
        }
        if (forwardThread != null){
            forwardThread.interrupt();
            forwardThread = null;
        }
        if (backwardThread != null){
            backwardThread.interrupt();
            backwardThread = null;
        }
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        shutDownAllThread();
    }


}
