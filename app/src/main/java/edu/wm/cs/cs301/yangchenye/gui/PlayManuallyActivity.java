package edu.wm.cs.cs301.yangchenye.gui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import edu.wm.cs.cs301.yangchenye.generation.Maze;

/**
 * This class has the responsibility to handle the maze playing process with the user manually
 * operating the robot
 *
 * @author Yangchen Ye
 */
public class PlayManuallyActivity extends AppCompatActivity{

    /**
     * the maze configuration
     */
    private Maze maze;

    /**
     * the state object that handles user interaction
     */
    private State statePlaying;

    /**
     * the robot object to play the maze
     */
    private Robot robot;

    /**
     * handler for posting ui update
     */
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_manually);

        //retrieve maze configuration and destroy the global reference
        maze = GeneratingActivity.maze;
        GeneratingActivity.maze = null;
        handler = new Handler();

        //set up ui
        setUpToolBar();

        //set up game
        initStatePlayingAndRobot();

        statePlaying.start(this, (MazePanel)findViewById(R.id.view));
    }

    private void setUpToolBar(){
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    /**
     * initialize the state of the game
     */
    private void initStatePlayingAndRobot(){

        ProgressBar progressBar =  findViewById(R.id.progressBar_energy);
        TextView text = findViewById(R.id.energy);
        //set up game
        statePlaying = new StatePlaying();
        ((StatePlaying) statePlaying).driverMode = false;
        robot = new BasicRobot();
        statePlaying.setMazeConfiguration(maze);
        statePlaying.setRobot(robot);
        statePlaying.setHandler(handler);
        statePlaying.setProgressBar(progressBar);
        statePlaying.setTextView(text);
        //set up robot
        robot.setMaze(statePlaying);

    }

    /**
     * This game has been won; navigate to the winning screen
     */
    public void navigateToWinning(){
        Log.v("Game Won", "Navigate to Winning Activity");

        Intent intent = new Intent(this, WinningActivity.class);
        transferEndInformation(intent);
        startActivity(intent);

        finish(); //finish this activity to clear the stack
    }

    /**
     * game is lost, navigate to according activity
     */
    public void navigateToLosing(){
        Log.v("Game Lost", "Navigate to Winning Activity");

        Intent intent = new Intent(this, LosingActivity.class);
        transferEndInformation(intent);
        startActivity(intent);

        finish(); //finish this activity to clear the stack
    }

    /**
     * helper method to transfer the energy, pathlength and shortest possible path via the intent
     * @param intent
     */
    private void transferEndInformation(Intent intent){
        intent.putExtra(Constants.ENERGY_CONSUMPTION_KEY, robot.getEnergyConsumption());
        intent.putExtra(Constants.PATH_LENGTH_KEY, robot.getOdometerReading());
        int[] startPosition = maze.getMazedists().getStartPosition();
        int[] exitPosition = maze.getMazedists().getExitPosition();
        int shortestPossiblePath = Math.abs(startPosition[0]-exitPosition[0]) + Math.abs(startPosition[1] - exitPosition[1]) + 1;
        intent.putExtra(Constants.SHORTEST_POSSIBLE_PATH_KEY,
                shortestPossiblePath);
    }

    /////////////////////Button for playing the maze///////////////////

    /**
     * respond to user's click of the up-moving button; make robot move one step forward
     * @param view
     */
    public void upButtonClicked(View view) {

        Log.v("up", "Robot Move Forward");
        new Thread(new Runnable() {
            @Override
            public void run() {
                robot.move(1);
            }
        }).start();

    }

    /**
     * respond to user's click of the left button; make robot rotate left
     * @param view
     */
    public void leftButtonClicked(View view){

        Log.v("left", "Robot Rotate Left");
        new Thread(new Runnable() {
            @Override
            public void run() {
                robot.rotate(Robot.Turn.LEFT);
            }
        }).start();

    }

    /**
     * respond to user's click of the left button; make robot rotate right
     * @param view
     */
    public void rightButtonClicked(View view){

        Log.v("right", "Robot Rotate Right");
        new Thread(new Runnable() {
            @Override
            public void run() {
                robot.rotate(Robot.Turn.RIGHT);
            }
        }).start();

    }

    /////////////////////Button for triggering map and solution//////////////////\
    /**
     * respond to user's click of the wall button; toggle wall display
     * @param view
     */
    public void wallButtonClicked(View view){

        Log.v("wall", "Toggle Seeable Walls");
        statePlaying.keyDown(Constants.UserInput.ToggleLocalMap, 0);

    }

    /**
     * respond to user's click of the wall button; toggle full map display
     * @param view
     */
    public void fullMapButtonClicked(View view){

        Log.v("fullMap", "Toggle Full Map");
        statePlaying.keyDown(Constants.UserInput.ToggleFullMap, 0);

    }

    /**
     * respond to user's click of the wall button; toggle solution display
     * @param view
     */
    public void solutionButtonClicked(View view){

        Log.v("solution","Toggle Solution");
        statePlaying.keyDown(Constants.UserInput.ToggleSolution, 0);

    }

    /**
     * respond to user's click of the scale up button
     * @param view
     */
    public void scaleUpButtonClicked(View view){

        Log.v("scaleUp","Scale Up Map");
        statePlaying.keyDown(Constants.UserInput.ZoomIn, 0);

    }

    /**
     * respond to user's click of the scale down button
     * @param view
     */
    public void scaleDownButtonClicked(View view){

        Log.v("scaleDown","Scale Down Map");
        statePlaying.keyDown(Constants.UserInput.ZoomOut, 0);

    }

}
