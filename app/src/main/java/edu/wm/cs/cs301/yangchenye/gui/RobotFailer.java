package edu.wm.cs.cs301.yangchenye.gui;

import edu.wm.cs.cs301.yangchenye.gui.Robot.Direction;

/**
 * This class encapsulates the functionality of triggering robot's sensor failure and repairing failed
 * sensor in a different thread with a time interval of 3 second
 *
 * @author yangchen
 *
 */
public class RobotFailer implements Runnable {

    private Robot robot;
    private Direction d;

    public RobotFailer(Robot robot, Direction d) {
        this.robot = robot;
        this.d = d;
    }

    @Override
    public void run() {

        while (!robot.hasStopped() && !robot.hasWon()) {

            if (robot.hasOperationalSensor(d))
                robot.triggerSensorFailure(d);
            else
                robot.repairFailedSensor(d);

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e1) {
                robot.repairFailedSensor(d);
                return;
            }

        }

    }

}

