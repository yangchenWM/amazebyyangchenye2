package edu.wm.cs.cs301.yangchenye.gui;

import android.annotation.SuppressLint;

import java.util.HashMap;

/**
 * This class contains all constants that are used in the maze package 
 * and shared among several classes.
 * 
 * @author Peter Kemper
 *
 */
public class Constants {

	//key for intent passing
	static final String BUILDER_KEY = "edu.wm.cs.cs301.yangchenye.gui.BUILDER";
	static final String DRIVER_KEY = "edu.wm.cs.cs301.yangchenye.gui.DRIVER";
	static final String SKILL_LEVEL_KEY = "edu.wm.cs.cs301.yangchenye.gui.SKILL";
	static final String GENERATE_NEW_MAZE = "edu.wm.cs.cs301.yangchenye.gui.NEWMAZE";

	static final String ENERGY_CONSUMPTION_KEY = "edu.wm.cs.cs301.yangchenye.gui.ENERGY";
	static final String PATH_LENGTH_KEY = "edu.wm.cs.cs301.yangchenye.gui.PATHLENGTH";
	static final String SHORTEST_POSSIBLE_PATH_KEY = "edu.wm.cs.cs301.yangchenye.gui.SHORT";

	// The panel used to display the maze has a fixed dimension
	static final int VIEW_WIDTH = 1000;
	static final int VIEW_HEIGHT = 1000;
	public static final int MAP_UNIT = 250;
	static final int VIEW_OFFSET = MAP_UNIT/8;
	static final int STEP_SIZE = MAP_UNIT/4;
	// Skill-level 
	// The user picks a skill level between 0 - 9, a-f 
	// The following arrays transform this into corresponding dimensions (x,y) for the resulting maze as well as the number of rooms and parts
	public static int[] SKILL_X =     { 4, 12, 15, 20, 25, 25, 35, 35, 40, 60, 70, 80, 90, 110, 150, 300 };
	public static int[] SKILL_Y =     { 4, 12, 15, 15, 20, 25, 25, 35, 40, 60, 70, 75, 75,  90, 120, 240 };
	public static int[] SKILL_ROOMS = { 0,  2,  2,  3,  4,  5, 10, 10, 20, 45, 45, 50, 50,  60,  80, 160 };
	public static int[] SKILL_PARTCT = { 60, 600, 900, 1200, 2100, 2700, 3300,
	5000, 6000, 13500, 19800, 25000, 29000, 45000, 85000, 85000*4 };
	
	// Possible user input  
	public enum UserInput {ReturnToTitle, Start, Up, Down, Left, Right, Jump, ToggleLocalMap, ToggleFullMap, ToggleSolution, ZoomIn, ZoomOut,
							Sensor, RepairSensor, FailSensor}

	// fixing a value matching the escape key
	final static int ESCAPE = 27;


	/**
	 * Lists all maze generation algorithms that are supported
	 * by the maze factory
	 *
	 * Refactored the enumeration class to support a mapping between
	 * builder and integer value
	 *
	 */
	public enum Builder {
		DFS(0),
		Prim(1),
		Kruskal(2),
		Eller(3),
		HAK(4),
		UNKNOWN(5);

		private int value;
		@SuppressLint("UseSparseArrays")
		private static HashMap<Integer, Builder> map = new HashMap<Integer, Builder>();

		Builder(int value) {
			this.value = value;
		}

		static {
			for (Builder builder : Builder.values()) {
				map.put(builder.value, builder);
			}
		}

		public static Builder getBuilder(int builderType) {
			return map.get(builderType);
		}

		public int getValue() {
			return value;
		}

	}

	/**
	 * list of all robot driver algorithms
	 *
	 * Refactored to support the mapping between enumeration type
	 * and integer
	 */
	public enum Driver{
		Manual(0),
		WallFollower(1),
		Wizard(2);

		private final int value;
		@SuppressLint("UseSparseArrays")
		private static HashMap<Integer, Driver> map = new HashMap<Integer, Driver>();

		Driver(int value){
			this.value  = value;
		}

		static {
			for (Driver driver : Driver.values()){
				map.put(driver.value, driver);
			}
		}

		public static Driver getDriver(int driverType){
			return map.get(driverType);
		}

		public final int getValue(){
			return value;
		}
	}
}
