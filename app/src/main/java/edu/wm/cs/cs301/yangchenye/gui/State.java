package edu.wm.cs.cs301.yangchenye.gui;

import android.content.Context;
import android.os.Handler;
import android.widget.ProgressBar;
import android.widget.TextView;

import edu.wm.cs.cs301.yangchenye.generation.CardinalDirection;
import edu.wm.cs.cs301.yangchenye.gui.Constants.UserInput;
import edu.wm.cs.cs301.yangchenye.generation.Maze;

/**
 * The state interface is used for the controller
 * such that all states of the UI conform and can
 * be treated the same way.
 *
 * 2019.10.11 Modified by yangchen to incorporate robot-related feature
 *
 * @author Peter Kemper
 *
 */
public interface State {
    /**
     * Starts the main operation of to be performed in this state.
     * Semantics depends on the particular state implementing it.
     * This is polymorphism in action.
     *
     * @param context is a reference to the current context
     * @param panel is the panel to draw graphics on
     */
    void start(Context context, MazePanel panel);

    /**
     * Sets the maze to play, the maze configuration is a
     * container that holds all necessary information about
     * a particular maze that can be played.
     * It is the outcome of the maze generation process
     * performed in the maze factory.
     * @param config gives a maze to play
     */
    void setMazeConfiguration(Maze config);

    /**
     * Provides input the user has given with the keyboard.
     * The SimpleKeyListener maps input into UserInput
     * and filters out invalid input.
     * @param key input from Enum set of legal commands
     * @param value carries a value, typically the skill level, optional
     * @return
     */
    boolean keyDown(UserInput key, int value);

    /**
     * set the reference to the robot
     * @param robot
     */
    void setRobot(Robot robot);

    /**
     * receive a refernce to the robot driver(if any) to drive the state
     * @param driver
     */
    //void setDriver(RobotDriver driver);

    /**
     * return a reference to the maze
     * @return
     */
    Maze getMazeConfig();

    /**
     * return the current position in the maze
     * @return
     */
    int[] getCurrentPosition();

    /**
     * return the current direction
     * @return
     */
    CardinalDirection getCurrentDirection();

    void setProgressBar(ProgressBar progressBar);

    void setHandler(Handler handler);

    void setTextView(TextView text);



}

