package edu.wm.cs.cs301.yangchenye.gui;

import androidx.appcompat.app.AppCompatActivity;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SeekBar;

/**
 *
 * This class is responsible for carrying out the title screen of the maze application. It displays
 * the title UI and receives user input for maze generation and playing preference, and the pass them
 * to the generating activity when the user presses start.
 *
 * @author Yangchen Ye
 */
public class AMazeActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, SeekBar.OnSeekBarChangeListener{

    //maze application preference selected by users
    /**
     * which builder to use; default as DFS
     */
    private int builderType = 0;
    /**
     * which driver algorithms to use, default as manual
     */
    private int driverType = 0;
    /**
     * complexity of the maze, default at 0
     */
    private int skillLevel = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a_maze);
        //set up UI
        setUpSpinners();
        setUpSeekBar();

    }

    private void setUpSpinners(){
        //set up spinner
        Spinner builder = findViewById(R.id.builderSpinner);
        //create an array adapter by the string array and a default spinner layout
        ArrayAdapter<CharSequence> builderAdapter = ArrayAdapter.createFromResource(this,
                R.array.builderArray, android.R.layout.simple_spinner_dropdown_item);
        //add the adapter to the spinner
        builder.setAdapter(builderAdapter);
        builder.setOnItemSelectedListener(this);

        Spinner driver = findViewById(R.id.driverSpinner);
        //create an array adapter by the string array and a default spinner layout
        ArrayAdapter<CharSequence> driverAdapter = ArrayAdapter.createFromResource(this,
                R.array.driverArray, android.R.layout.simple_spinner_dropdown_item);
        //add the adapter to the spinner
        driver.setAdapter(driverAdapter);
        driver.setOnItemSelectedListener(this);
    }

    private void setUpSeekBar(){
        //set up seek bar
        SeekBar skill = findViewById(R.id.skillLevel);
        skill.setOnSeekBarChangeListener(this);
    }

    /**
     * helper method to transfer user selection via the intent
     * @param intent
     * @param newMaze
     */
    private void transferMazePreference(Intent intent, boolean newMaze){
        //send information
        intent.putExtra(Constants.BUILDER_KEY, builderType);
        intent.putExtra(Constants.DRIVER_KEY, driverType);
        intent.putExtra(Constants.SKILL_LEVEL_KEY, skillLevel);

        intent.putExtra(Constants.GENERATE_NEW_MAZE, newMaze); //indicating load an existing maze or not
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        //retrieve selected item
        String selected = (String) adapterView.getItemAtPosition(i);
        Log.v("Selection: ", selected);

        switch(selected){
            case "DFS":
                builderType = Constants.Builder.DFS.getValue();
                break;
            case "Prim":
                builderType = Constants.Builder.Prim.getValue();
                break;
            case "Eller":
                builderType = Constants.Builder.Eller.getValue();
                break;
            case "Kruskal":
                builderType = Constants.Builder.Kruskal.getValue();
                break;
            case "HAK":
                builderType = Constants.Builder.HAK.getValue();
                break;
            case "Manual":
                driverType = Constants.Driver.Manual.getValue();
                break;
            case "WallFollower":
                driverType = Constants.Driver.WallFollower.getValue();
                break;
            case "Wizard":
                driverType = Constants.Driver.Wizard.getValue();
                break;
            default:
                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        //nothing to do
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        skillLevel = i;
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        //nothing to do
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        Log.v("skillLevelSelection", "Selected Skill Level: " + skillLevel);
    }

    /**
     * This method wraps up the current maze application preference and send them together to
     * generating activity and starts maze generation. This method is called when the user clicked
     * start.
     *
     * @param view
     */
    public void startButtonClicked(View view){

        Log.v("Start", "Start Navigating To Generating Activity");

        Intent intent = new Intent(this, GeneratingActivity.class);

        //send information
        transferMazePreference(intent, true);

        startActivity(intent);

    }

    /**
     *This method is called when the user clicked revisit, navigate to the
     * generating activity with information that the maze should be loaded from
     * a file
     *
     * @param view
     */
    public void revisitButtonClicked(View view){

        Log.v("Revisit", "Start Navigating To Generating Activity");

        Intent intent = new Intent(this, GeneratingActivity.class);

        //send information
        transferMazePreference(intent, false);

        startActivity(intent);

    }



}
