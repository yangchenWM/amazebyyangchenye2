package edu.wm.cs.cs301.yangchenye.gui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.transition.Slide;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

/**
 * This class has the responsibility to display the winning screen and tell user tap anywhere to
 * restart the game
 *
 * @author Yangchen Ye
 */
public class WinningActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_winning);

        setUpText();

    }

    /**
     * set up information to show on the end screen
     */
    private void setUpText(){
        TextView energy = findViewById(R.id.energy_consumed);
        TextView pathlength = findViewById(R.id.pathlength);
        TextView shortest  = findViewById(R.id.shortestPath);

        //set information
        energy.setText(String.valueOf(getIntent().getIntExtra(Constants.ENERGY_CONSUMPTION_KEY, 0)));
        pathlength.setText(String.valueOf(getIntent().getIntExtra(Constants.PATH_LENGTH_KEY, 0)));
        shortest.setText(String.valueOf(getIntent().getIntExtra(Constants.SHORTEST_POSSIBLE_PATH_KEY, 0)));
    }

    /**
     * user tapping anywhere would lead to the main activity, the same effect as pressing the back
     * @param view
     */
    public void returnToTitle(View view){

        Log.v("Tap: ", "Game Restart");

        super.onBackPressed();

        finish();

    }


}
